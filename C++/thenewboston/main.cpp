#include <iostream>
#include "Burrito.h"
#include "cstdlib"
#include "birthday.h"
#include "people.h"
#include "Sally.h"
using namespace std;
class Name{
public :
    void setName(string x){
        name = x;
    }
    string getName(){
        return name;
    }
private:
    string name;
};

void passByValue(int x){
    x  = 3;
};
void passByReference(int &x){
    x = 33;
};

int main()
{
    Sally a(34);
    Sally b(24);
    Sally c;
    c = b+a;
    cout << c.num << endl;

}
