package cs520hw1
abstract class BatAndBallGames{
    public int playersPerTeam;
    public int lengthOfGame;
    public int gamescores1;
    public int gamescores2;
    
    public void display(){};
    public void rulesSummary(){};
    public void team1(){};
    public void team2(){};
    
}