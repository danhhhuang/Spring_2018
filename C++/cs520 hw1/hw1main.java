import cs520hw1.*;
class Main {
  public static void main(String[] args) {
    baseball n1 = new baseball();
    softball n2 = new softball();
    cricket n3 = new cricket();
    n1.rulesSummary();
    n1.display();
    n2.rulesSummary();
    n2.display();
    n3.rulesSummary();
    n3.display();
  }
}
