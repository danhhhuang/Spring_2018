#include <iostream>
using namespace std;


class BatAndBallGames{
  public:
  int playersPerTeam;
  int lengthOfGame;
  int gamescores1;
  int gamescores2;
  virtual void display()=0;
  virtual void rulesSummary()=0;
  virtual void team1()=0;
  virtual void team2()=0;

};
class Baseball:public BatAndBallGames{
  public:
      int playersPerTeam = 9;
      int lengthOfGame =2;
      int gamescores1 = 0;
      int gamescores2 =4;
      int foulout = 3;
      int balls =27;
      int hits =27;
  void display(){
    cout << "the scores of this baseball game is " <<gamescores1<<":"<<gamescores2 << endl;
    cout << "the length of this baseball game should be nine innings and right now its at inning #"<< lengthOfGame<<endl;
    cout << "the number of players been foul out is " << foulout << endl;
  }
  void rulesSummary(){
    cout << "---------------------------------------------------------------------------------------------" << endl;
    cout << "Rules for baseball :A game is played between two teams, each composed of nine players, that take turns playing offense (batting and baserunning) and defense (pitching and fielding). A pair of turns, one at bat and one in the field, by each team constitutes an inning. A game consists of nine innings (seven innings at the high school level and in doubleheaders in college and minor leagues)" << endl;
    cout << "---------------------------------------------------------------------------------------------" << endl;
  }
  void team1(){
      cout <<"The two teams compete with eachother are : \'RED Sox\' and \'Yankees\' " << endl;
  }
  void team2(){
      cout <<"The two teams compete with eachother are : \'RED Sox\' and \'Yankees\' " << endl;
  }
  void numOfBallsThrown(){
     cout << "The number of Balls been thrown is "<< balls <<endl;
  }
  void numOfHits(){
    cout <<" The number of hits is " <<hits<<endl;
}
};

class Softball:public BatAndBallGames{
  public:
    int gamescores1 =12;
    int gamescores2 =8;
    int lengthOfGame=6;
    int playersPerTeam=10;
    int ticket=288;
    int numofcel=24;
    void display(){
    cout << "the scores of this softball game is " <<gamescores1<<":"<<gamescores2 << endl;
    cout << "the length of this softball game should be seven innings and right now its at inning #"<<lengthOfGame<<endl;

  }
    void rulesSummary(){
    cout << "---------------------------------------------------------------------------------------------" << endl;
    cout << "rules for softball Each softball team has 9 players. The game takes place over 7 innings and within each innings the team will bat then field. An innings is split"
    << "into two sections called the top and the bottom of the innings. The away team bats first at the top of the innings whilst the home team field, then teams switch so the home team bats at the bottom of the innings."
<< "Pitch sizes vary in softball and are often just whatever size can be found or used at the time. The in- filed has four bases in a diamond shape. The bases are home base (where the batter stands),"
 <<"first base, second base and third base. Home plate can be found in the centre of the field where the pitcher must stand to throw the ball. The pitcher must throw the ball underarm and must have at"
 <<"least one foot on the plate at the point of delivery."<<endl;
    cout << "---------------------------------------------------------------------------------------------" << endl;
  }
   void team1(){
      cout <<"The two teams compete with eachother are : \'Bisons\' and \'Storm\' " << endl;
  }
  void team2(){
     cout <<"The two teams compete with eachother are : \'Bisons\' and \'Storm\' " << endl;
  }
  void ticketsold(){
      cout <<"The number of ticket sold is :" << ticket << endl;
  }
  void numofCel(){
      cout <<"The number of celebrities come at this game is: " <<numofcel << endl;
  }
};
class Cricket:public BatAndBallGames{
  public:
    int gamescores1 =247;
    int gamescores2 =0;
    int lengthOfGame=5;
    int playersPerTeam=12;
    int TeamdefbyEngland=13;
    int TeamdefbyIndia=10;
  void display(){
    cout << "the scores of this cricket game is " <<gamescores1<< endl;
    cout << "Right now its at overs #"<<lengthOfGame<<endl;

  }
    void rulesSummary(){
    cout << "---------------------------------------------------------------------------------------------" << endl;
    cout << "Cricket is a bat-and-ball game played on a cricket field (see image, right) between two teams of eleven players each.The field is usually circular or oval in shape and the edge of the playing area is marked by a boundary, which may be a fence, part of the stands, a rope, a painted line"
    << "or a combination of these; the boundary must if possible be marked along its entire length."
    << "In he approximate centre of the field is a rectangular pitch (see image, below) on which a wooden target called a wicket is sited at each end; the wickets are placed 22 yards (20 m) apart.The pitch is a flat surface 3 metres (9.8 ft) wide, with very short grass that tends to be worn away as the"
    << "game progresses (cricket can also be played on artificial surfaces, notably matting). Each wicket is made of three wooden stumps topped by two bails.\n" <<endl;
    cout << "---------------------------------------------------------------------------------------------" << endl;

  }
   void team1(){
      cout <<"The two teams compete with eachother are : \'England cricket team\' and \'India national cricket team\' " << endl;
  }
  void team2(){
     cout <<"The two teams compete with eachother are : \'England cricket team\' and \'India national cricket team\' " << endl;
  }
  void EnglandWons(){
      cout <<"The number of teams defeated by England is :"<< TeamdefbyEngland<<endl;
  }
  void IndiaWons(){
      cout <<"The number of teams defeated by India is :"<< TeamdefbyIndia<<endl;
  }
};

int main() {
  Baseball bobj;
  bobj.rulesSummary();
  bobj.display();
  bobj.team1();


  Softball sobj;
  sobj.rulesSummary();
  sobj.display();
  sobj.ticketsold();

  Cricket cobj;
  cobj.rulesSummary();
  cobj.display();


}
